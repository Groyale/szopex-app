import { Injectable } from '@nestjs/common';
import { DB } from './app.db';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello, API!';
  }

  public getAllProducts(): any {
    return DB;
  }

  public getProductByHandle(handle: string): any {
    return DB.find(o => o.Handle === handle);
  }
}
