import { Controller, Get, Param } from '@nestjs/common';
import { AppService } from './app.service';

@Controller('api')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('products')
  public getAllProducts(): any {
    return this.appService.getAllProducts();
  }

  @Get('product/:handle')
  public getProductByHandle(@Param('handle') handle: string): any {
    return this.appService.getProductByHandle(handle);
  }
}
