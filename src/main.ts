import { HttpsOptions } from '@nestjs/common/interfaces/external/https-options.interface';
import { NestFactory } from '@nestjs/core';
import * as fs from 'fs';
import { AppModule } from './app.module';

const httpsOptions: HttpsOptions = {
  key: fs.readFileSync('/etc/letsencrypt/live/wunderwegner.pl/privkey.pem'),
  cert: fs.readFileSync('/etc/letsencrypt/live/wunderwegner.pl/fullchain.pem'),
};

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { httpsOptions });
  app.enableCors();
  await app.listen(3000);
}

bootstrap();
